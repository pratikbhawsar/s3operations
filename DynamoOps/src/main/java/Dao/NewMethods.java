/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;
import Model.StudentMarks;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import java.util.Map;

import java.util.HashMap;
import java.util.Set;


/**
 *
 * @author pratik.bhawsar
 */
public class NewMethods {
    public Boolean  insertMarks(StudentMarks sm)
    {
        int flag=0;
        HashMap<String,AttributeValue> item_values =
            new HashMap<>();
            item_values.put("Rollno", new AttributeValue(sm.getRollno()));
            item_values.put("English", new AttributeValue(sm.getEnglish()));
            item_values.put("Science", new AttributeValue(sm.getScience()));
            item_values.put("Maths", new AttributeValue(sm.getMaths()));
            item_values.put("History", new AttributeValue(sm.getHistory()));
        final AmazonDynamoDB ddb = AmazonDynamoDBClientBuilder.defaultClient();
        String table="marklist";
        try {
            ddb.putItem(table, item_values);
            flag=1;
        } catch (ResourceNotFoundException ex) {
            System.err.format("Error: The table \"%s\" can't be found.\n", table);
            System.err.println("Be sure that it exists and that you've typed its name correctly!");
            System.exit(1);
        } catch (AmazonServiceException ex) {
            System.err.println(ex.getMessage());
            System.exit(1);
        }
        System.out.println("Done!");
        if(flag==1)
            return true;
        else
            return false;
    }
    
    public  Boolean listMarks(String rno)
    {
        String table_name="marklist";
        System.out.format("Retrieving item \"%s\" from \"%s\"\n",
                rno, table_name);
HashMap<String,AttributeValue> key_to_get =
    new HashMap<>();

key_to_get.put("Rollno", new AttributeValue(rno));

GetItemRequest request = null;
    request = new GetItemRequest()
        .withKey(key_to_get)
        .withTableName(table_name);


final AmazonDynamoDB ddb = AmazonDynamoDBClientBuilder.defaultClient();

try {
    Map<String,AttributeValue> returned_item =
       ddb.getItem(request).getItem();
    if (returned_item != null) {
        Set<String> keys = returned_item.keySet();
        for (String key : keys) {
            System.out.format("%s: %s\n",
                    key, returned_item.get(key).toString());
        }
    } else {
        System.out.format("No item found with the key %s!\n",rno);
    }
} catch (AmazonServiceException e) {
    System.err.println(e.getErrorMessage());
    System.exit(1);

        
    }
return true;
    }
}
