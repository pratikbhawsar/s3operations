package Dao;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class MainClass {
	public static void main(String[] args)  {
           Scanner sc = new Scanner(System.in);
            System.out.println("Enter Roll no");
            String rno=sc.next();
            System.out.println("Enter english marks");
            String e=sc.next();
            System.out.println("Enter science marks");
            String s=sc.next();
            System.out.println("Enter maths marks");
            String m=sc.next();
            System.out.println("Enter history marks");
            String h=sc.next();
            HashMap<String,AttributeValue> item_values =
            new HashMap<String,AttributeValue>();
            item_values.put("Rollno", new AttributeValue(rno));
            item_values.put("English", new AttributeValue(e));
            item_values.put("Science", new AttributeValue(s));
            item_values.put("Maths", new AttributeValue(m));
            item_values.put("History", new AttributeValue(h));
        final AmazonDynamoDB ddb = AmazonDynamoDBClientBuilder.defaultClient();
        String table="marklist";
        try {
            ddb.putItem(table, item_values);
        } catch (ResourceNotFoundException ex) {
            System.err.format("Error: The table \"%s\" can't be found.\n", table);
            System.err.println("Be sure that it exists and that you've typed its name correctly!");
            System.exit(1);
        } catch (AmazonServiceException ex) {
            System.err.println(ex.getMessage());
            System.exit(1);
        }
        System.out.println("Done!");

	}
}
