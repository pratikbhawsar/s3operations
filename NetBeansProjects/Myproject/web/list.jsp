<%@page import="Model.Employee"%>
<%@page import="java.util.List"%>
<%@page import="Dao.EmpMethods"%>

<%@include  file="index.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search All</title>
        <%! 
          List<Employee>mylist;
        %>
    </head>
    <body>
      <center>  
        <%
           EmpMethods em=new EmpMethods();
           mylist= em.ListAll();
        %>
        
        <table border="1">
             <tr><th>Emp Id</th><th>Name</th><th>Salary</th><th>Experience</th></tr>
          <% 
               for(Employee E:mylist)
               {    
          %>
              <tr>
                 <td><%=E.getEmpId()%></td>
                 <td><%=E.getEmpName()%></td>
                 <td><%=E.getSalary()%></td>
                 <td><%=E.getEmpExp()%></td>
             </tr>
             
           <% 
               }
           %>  
             
        </table>
       </center>      
    </body>
</html>
