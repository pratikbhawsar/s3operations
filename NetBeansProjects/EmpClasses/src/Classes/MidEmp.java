/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Interfaces.EmpHead;

/**
 *
 * @author pratik.bhawsar
 */
public class MidEmp implements EmpHead{
     private static int id;
     private static int salary;
     private static String name;
     private static int Exp=2 ;
     private static int Certification=2;

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        MidEmp.id = id;
    }

    public static int getSalary() {
        return salary;
    }

    public static void setSalary(int salary) {
        MidEmp.salary = salary;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        MidEmp.name = name;
    }

    public static int getExp() {
        return Exp;
    }

    public static void setExp(int Exp) {
        MidEmp.Exp = Exp;
    }

    public static int getCertification() {
        return Certification;
    }

    public static void setCertification(int Certification) {
        MidEmp.Certification = Certification;
    }
     @Override
    public int id(int i)
    {
       return id;
    }
     @Override
    public int salary(int s)
    {
       return salary;    
    }
     @Override
    public String name(String n)
    {
        return name;
    }
     @Override
    public int Exp(int exp)
    {
        return Exp;
    }
}
