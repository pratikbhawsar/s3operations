/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collections;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author pratik.bhawsar
 */
public class Collections {
    public static void main(String[] args) {
        ArrayList<String> list1 = new ArrayList<String>();
        Scanner sc= new Scanner(System.in);
        //Adding something to the list
        System.out.println("Enter the no. of names you want to enter:-\n");
        int no=sc.nextInt();
        for (int i = 0; i < no; i++) {
            String name=sc.next();
            list1.add(name);
        }
        //Printing the list
        System.out.println("\nThe list of names is:\n");
        for(String str:list1)
        {
            System.out.println(str);
        }
    }
    
}
