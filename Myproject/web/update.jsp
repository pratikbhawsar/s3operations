
<%@page import="Model.Employee" %>
<%@page import="Dao.EmpMethods" %>
<%@page  session="true" %>
<%@include  file="index.jsp" %>

<html>
    <head>
        <title>Update Employee</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%! 
        Employee E;
     %>
    
    </head>
    <body>
      
         <center>
             <h3>Update By Employee ID</h3>
        <form method="post" action="update.jsp">
            <table border="1">
                
             <tr>
                 <th>Emp ID:</th>
                 <td><input type="text" name="textid"></td>  
             </tr>
             <tr>
                 <td></td>
              <td>
                  <input type="submit" value="Submit">
                  <input type="reset" value="Reset">
              </td>   
             </tr>
             
             </table>   
            
            
        </form>
        
<%
      int id;
      String no=null;
      no=request.getParameter("textid");
     if(no!=null)
     {
        id=Integer.parseInt(no);
         EmpMethods em=new EmpMethods();
         E=em.searchById(id);
         if(E==null)
             out.println("Record Not Found");
         else
         {
            session.setAttribute("id",E.getEmpId() );
            session.setAttribute("name",E.getEmpName() );
            session.setAttribute("salary",E.getSalary());
            session.setAttribute("exp",E.getEmpExp());
            response.sendRedirect("update1.jsp");
         }
          
     }
      
     %>        
        
</center>
    </body>
</html>
