<%@page import="Model.Employee"%>
<%@page import="Dao.EmpMethods"%>
<%@include  file="index.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <%! 
        String name;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Employee</title>
    </head>
    <body>
     
        <form method="post" action="update1.jsp">  
      <center>
          <h3>Update Employee Section</h3>
       <table border="1">
           <tr>
           <th>Emp ID</th>
           <td><input type="text" name="id" value=<%=session.getAttribute("id").toString()%> ></td> 
          </tr>
          <tr>
           <th>Name</th>
            <% 
              name=session.getAttribute("name").toString();
            %>           
          <td><input type="text" name="name" value=<%=name%>></td> 
          </tr>
          <tr>
           <th>Salary</th>
          <td><input type="text" name="salary" value=<%=session.getAttribute("salary").toString()%>></td> 
          </tr>
          <tr>
           <th>Experience</th>
          <td><input type="text" name="exp" value=<%=session.getAttribute("exp").toString()%>></td> 
          </tr>
          <tr>
        <tr>
            <td></td> 
          <td>
              <input type="submit" value="Submit">
              <input type="reset" value="Reset">
          </td> 
          </tr>  
       </table>
      </center>
      </form>   
     <% 
          int id,salary,exp;
          String name,no=null;
          
       try
     {  
        no=request.getParameter("id");
        if(no!=null)
       { 
          id=Integer.parseInt(no);
         name=request.getParameter("name");
         salary=Integer.parseInt(request.getParameter("salary"));
         exp=Integer.parseInt(request.getParameter("exp"));
         Employee E=new Employee();
         E.setEmpId(id);
         E.setEmpName(name);
         E.setSalary(salary);
         E.setEmpExp(exp);
         EmpMethods em=new EmpMethods();
         if(em.updateEmployee(E))
             out.println("Record Updated.......");
       }   
       }
      catch(Exception e)
      {
       out.println(e);
      }
      
          
          
     %>   
        
    </body>
</html>
