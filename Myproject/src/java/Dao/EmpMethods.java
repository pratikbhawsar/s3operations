package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Connection.JavaCon;
import Model.Employee;

public class EmpMethods {
     public Boolean insertEmployee(Employee e) throws SQLException 
     {
    	 Connection con=null;
    	 PreparedStatement ps=null;
    	 con=JavaCon.getConnection();
    	 String sql;
    	 sql="insert into employee values(?,?,?,?)";
    	 ps=con.prepareStatement(sql);
    	 ps.setInt(1,e.getEmpId());
    	 ps.setString(2,e.getEmpName());
    	 ps.setInt(3,e.getSalary());
    	 ps.setInt(4,e.getEmpExp());
    	 int temp =ps.executeUpdate();
    	 if(temp>0)
    		 return true;
    	 else
    		 return false;
    	 
     }
     public Boolean updateEmployee(Employee e) throws SQLException 
     {
    	 Connection con=null;
    	 PreparedStatement ps=null;
    	 con=JavaCon.getConnection();
    	 String sql; 
    	 sql="update employee set id=?,name=?,salary=?,exp=? where id=?";
    	 ps=con.prepareStatement(sql);
    	 ps.setInt(1,e.getEmpId());
    	 ps.setString(2,e.getEmpName());
    	 ps.setInt(3,e.getSalary());
    	 ps.setInt(4,e.getEmpExp());
         ps.setInt(5,e.getEmpId());
    	 int temp =ps.executeUpdate();
    	 if(temp>0)
    		 return true;
    	 else
    		 return false;
    	 
     }
     public boolean deleteById(int empId) throws Exception
     {
            Connection con=null;
            PreparedStatement ps=null;
            con=JavaCon.getConnection();
            String sql;
            sql="delete from employee where id=?";
            ps=con.prepareStatement(sql);
            ps.setInt(1, empId);
            if(ps.executeUpdate()>0)
                return true;
            else
                return false;
         
         
     }
     public List<Employee> ListAll() throws Exception
     {
        
            Connection con=null;
            PreparedStatement ps=null;
            ResultSet rs=null; 
            con=JavaCon.getConnection();
            String sql;
            sql="select * from Employee";
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
           List<Employee>mylist=new ArrayList<Employee>();
           while(rs.next())
           {
              Employee E=new Employee();
               E.setEmpId(rs.getInt(1));
               E.setEmpName(rs.getString(2));
               E.setSalary(rs.getInt(3));
               E.setEmpExp(rs.getInt(4));
               mylist.add(E);
           }   
            
           return mylist;
     }
     public Employee searchById(int id) throws Exception
        {
               Connection con=null;
               PreparedStatement ps=null;
               ResultSet rs=null; 
               con=JavaCon.getConnection();
               String sql;
               sql="select * from employee where id=?";
               ps=con.prepareStatement(sql);
               ps.setInt(1, id);
               rs=ps.executeQuery();
               Employee E=new Employee();
               if(rs.next())
               {
                  E.setEmpId(rs.getInt(1));
                  E.setEmpName(rs.getString(2));
                  E.setSalary(rs.getInt(3));
                  E.setEmpExp(rs.getInt(4));
               } 
               else
                   E=null;
               
               return E;
        
        
        }
}
