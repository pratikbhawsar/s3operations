<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <style>
.button {
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 15px 25px;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
}

.button:hover {
  background-color: green;
}
</style>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
    <a href="insert.jsp"><button class="button">Insert Employee</button></a>
    <a href="update.jsp"><button class="button">Update Employee</button></a>
    <a href="delete.jsp"><button class="button">Delete Employee</button></a>
    <a href="list.jsp"><button class="button">Show Employees</button></a>
    <a href="search.jsp"><button class="button">Search Employee</button></a>
    
</body>
</html>