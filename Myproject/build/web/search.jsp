
<%@page import="Model.Employee" %>
<%@page import="Dao.EmpMethods" %>
<%@page  session="true" %>
<%@include  file="index.jsp" %>

<html>
    <head>
        <title>Search Employee</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%! 
        Employee E;
     %>
    
    </head>
    <body>
      
         <center>
             <h3>Search By Employee ID</h3>
        <form method="post" action="search.jsp">
            <table border="1">
                
             <tr>
                 <th>Emp ID:</th>
                 <td><input type="text" name="textid"></td>  
             </tr>
             <tr>
                 <td></td>
              <td>
                  <input type="submit" value="Submit">
                  <input type="reset" value="Reset">
              </td>   
             </tr>
             
             </table>   
            
            
        </form>
        
<%
      int id;
      String no=null;
      no=request.getParameter("textid");
     if(no!=null)
     {
        id=Integer.parseInt(no);
         EmpMethods em=new EmpMethods();
         E=em.searchById(id);
         if(E==null)
             out.println("Record Not Found");
         else
         { %>
            <table border="1">
             <tr><th>Emp Id</th><th>Name</th><th>Salary</th><th>Experience</th></tr>
              <tr><br>
                 <td><%=E.getEmpId()%></td>
                 <td><%=E.getEmpName()%></td>
                 <td><%=E.getSalary()%></td>
                 <td><%=E.getEmpExp()%></td>
             </tr>
             
        </table>
           <% 
         }
          
     }
      
     %>        
        
</center>
    </body>
</html>
