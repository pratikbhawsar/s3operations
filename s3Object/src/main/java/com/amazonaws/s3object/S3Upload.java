/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amazonaws.s3object;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.Bucket;
import java.io.File;
import java.util.List;
import java.util.Scanner;
/**
 *
 * @author pratik.bhawsar
 */
public class S3Upload {
    public static void main(String[] args) {
      final AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();
        System.out.println("Select an operation:-");
        System.out.println("1.List Bucket \n 2. Upload Object. \n");
        Scanner sc=new Scanner(System.in);
        int Choice=sc.nextInt();
        
      switch(Choice)
      {
          case 1:{
              List<Bucket> buckets = s3.listBuckets();
              System.out.println("Your Amazon S3 buckets are:");
              for (Bucket b : buckets) {
              System.out.println("* " + b.getName());
          }
      }
          case 2:{
              
              File file = new File("C:\\Users\\pratik.bhawsar\\Downloads\\pom.txt");
              System.out.println("File ::"  + file.exists());
              String file_path="C:\\Users\\pratik.bhawsar\\Downloads\\pom.txt";
              String bucket_name="blazebucket123";
              String key_name="pom.txt";
              System.out.format("Uploading %s to S3 bucket %s...\n", file_path, bucket_name);
try {
    s3.putObject(bucket_name, key_name, new File(file_path));
} catch (AmazonServiceException e) {
    System.err.println(e.getErrorMessage());
    System.exit(1);
}
          }

}

  
    }
    
}
